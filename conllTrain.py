import sys;
sys.path.insert(0,"/home/jborst/tmp/utsi")
sys.path.insert(0,"/home/jborst/tmp/raziel")
sys.path.insert(0,"/home/jborst/tmp/ptagger")
from tqdm import tqdm
import numpy as np
import itertools
from utsi import DataManager
from raziel.Raziel import Raziel
dm = DataManager()
bb = Raziel(dm)



bb.data_manager.read_conll("/disk1/users/jborst/Data/Test/NER/CoNLL-2003/de/BIOES/train.txt", purpose="train")
bb.data_manager.read_conll("/disk1/users/jborst/Data/Test/NER/CoNLL-2003/de/BIOES/valid.txt", purpose="valid")
bb.data_manager.read_conll("/disk1/users/jborst/Data/Test/NER/CoNLL-2003/de/BIOES/test.txt", purpose="test")
tagset = ["PER", "ORG", "LOC", "MISC"]
bb.setTagset(tagset, scheme="iobes")
bb.language = "de"
bb.sequence_length = 140
bb.character_length = 60

traindata = bb.chain({
    "chars": ("token", "character", "index", None),
    "words":          ("token", "custom", "embedding", "/disk1/users/jborst/Data/Pretrained Word Embeddings/german/cc.de.300.10K.vec"), # only act on something as file
    "tags":    ("ner",   "multilabel", "index", "sparse" ),
    # "fasttext":     ("token", "fasttext", "embedding", "/disk1/users/jborst/Data/Pretrained Word Embeddings/german/cc.de.300.bin")
    # "ner_embedding": ("ner", "custom", "embedding", "/disk1/users/jborst/Data/Pretrained Word Embeddings/test.txt")
})


from ptagger.architectures.sequence.cnnx3_elmo_flair import build as model
pt.build(wordEmbedding="/disk1/users/jborst/Data/Pretrained Word Embeddings/german_unigram.txt",model=model, tagset=tagset)
print(pt.model.summary())
import pickle

pt.setLogging("error")
with open("/disk1/users/jborst/Data/Test/NER/CoNLL-2003/de/BIOES/valid_flairfasttexttuned_elmo_glove100.pickle", "rb") as output:
    valid=pickle.load( output)
with open("/disk1/users/jborst/Data/Test/NER/CoNLL-2003/de/BIOES/train_flairfasttexttuned_elmo_glove100.pickle", "rb") as output:
    train = pickle.load(output)

import keras
pt.wordBiLSTMUnits=256
pt.optimizer = keras.optimizers.SGD(lr=0.011,  clipvalue=5., decay=0.0)

import math
def step_decay(epoch):
    maximum = 0.02
    initial_lrate = maximum/400
    current = float(epoch)/100.
    if current < 0.2:
        lrate = epoch*epoch * initial_lrate
    else:
        lrate = maximum *(1/(1+6*current-1.6) )
    return lrate

import keras
lrate = keras.callbacks.LearningRateScheduler(step_decay)
es = keras.callbacks.EarlyStopping(monitor='val_acc', min_delta=0, patience=4, verbose=0,
                                   mode='auto', baseline=None, restore_best_weights=True)

pt.train(train,valid,save_best=True, batchSize=64, epochs=10)
pt.train(train,valid,save_best=True,batchSize=16, callbacks=[es,lrate])

with open("/disk1/users/jborst/Data/Test/NER/CoNLL-2003/de/BIOES/test_flairfasttexttuned_elmo_glove100.pickle", "rb") as output:
    test = pickle.load(output)


print(pt.evaluate(test,numeric=True))
pt.save("bugtest_save", compression=False)
#
# #
# # import keras
# # cb_tb = keras.callbacks.TensorBoard(log_dir='./Graph/test/',
# # 	                                    histogram_freq=10,
# # 	                                    write_graph=True,
# # 	                                    write_images=False,
# # 	                                    )
# # pt.train2stage(train, valid, callbacks=[cb_tb])
# import keras
# pt.train(train, valid,callbacks=[
#     keras.callbacks.EarlyStopping(monitor="val_acc",min_delta = 0, mode = "max", patience=10)], name="german_3cnn",save_best=True)
# e = pt.evaluate(test, "evaluate.txt")
#
# import numpy as np
# pt.reset()
# prediction = pt.model.predict(test)
# tags = [pt.vocab.getTagsetIndexReverse([np.where(r == 1)[0][0] for r in sentence[0]][0:sentence[1]]) for sentence in zip(prediction, test["lengths"])]
# truth = test["tagsRaw"]
# pt.logger.debug("Writing to file")
# for sentence in zip(test["wordsRaw"], truth, tags):
#     for w, t, p in zip(sentence[0], sentence[1], sentence[2]):
#         print(w + " " + t + " " + p)
#
# any(["None" in x for x in tags])
#
# for sentence in zip(test["wordsRaw"], truth, tags):
#     for w, t, p in zip(sentence[0], sentence[1], sentence[2]):
#         if w is None or t is None or p is None:
#             print(sentence)